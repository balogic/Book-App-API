Category.create([
  {
    title: 'Read'
  },
  {
    title: 'Currently Reading'
  },
  {
    title: 'To Read'
  }
  ])

  Book.create([
    {
      title: 'Alchemist',
      category_id: 1,
      author: 'Paulo'
    },
    {
      title: 'Blink',
      category_id: 2,
      author: 'Malcom Gladwell'
    },
    {
      title: 'Who is Shivaji?',
      category_id: 3,
      author: 'Govind Bansare'
    }
    ])
