class CategoryController < ApplicationController
  def index
    @categories = Category.all
    Rails.logger.debug {@categories.inspect}
    render json: @categories, status: :ok
  end

  def show
    @category = Category.find(params[:id])
    render json: @category, status: :ok
  end
end
