Rails.application.routes.draw do
  get '/categories' => 'category#index'
  get '/categories/:id' => 'category#show'
  get '/books' => 'books#index'
  get '/books/:id' => 'books#show'
end
